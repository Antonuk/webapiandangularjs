﻿(function () {
    'use strict';

    /*
    var moviesServices = angular.module('moviesServices', ['ngResource']);
    moviesServices.factory('Movies', ['$resource',
        function ($resource) {
            return $resource('/api/movies', {}, {
                query: { method: 'GET', params: {}, isArray: true }
            });
        }]);
    
    */
    angular
        .module('moviesServices', ['ngResource'])
        .factory('Movie', Movie);

    Movie.$inject = ['$resource'];

    function Movie($resource) {
        return $resource('/api/movies/:id');
    };
    console.log('Service init');
})();