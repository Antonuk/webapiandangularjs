﻿(function () {
    'use strict';

    angular
        //set angular modul here from app.js
        .module('moviesApp')
        .controller('MoviesListController', MoviesListController)
        .controller('MoviesAddController', MoviesAddController)
        .controller('MoviesEditController', MoviesEditController)
        .controller('MoviesDeleteController', MoviesDeleteController);
        //.controller('moviesController', moviesController);

    /*
    //service depency injection 
    moviesController.$inject = ['$scope', 'Movies']; 

    function moviesController($scope, Movies) {
        $scope.movies = Movies.query();
    }
    */

    //list c-r
    MoviesListController.$inject = ['$scope', 'Movie']; //model name 'Movie' defined in service
    function MoviesListController($scope, Movie) {
        $scope.movies = Movie.query();
    };

    //add c-r
    MoviesAddController.$inject = ['$scope', '$location', 'Movie'];
    function MoviesAddController($scope, $location, Movie) {
        $scope.movie = new Movie();
        $scope.add = function () {
            $scope.movie.$save(function () {
                $location.path('/');
            });
        };
    };

    //edit c-r
    MoviesEditController.$inject = ['$scope', '$routeParams', '$location', 'Movie'];
    function MoviesEditController($scope, $routeParams, $location, Movie) {
        $scope.movie = Movie.get({ id: $routeParams.id });
        $scope.edit = function () {
            $scope.movie.$save(function () {
                $location.path('/');
            });
        };
    };

    //delete c-r
    MoviesDeleteController.$inject = ['$scope', '$routeParams', '$location', 'Movie'];

    function MoviesDeleteController($scope, $routeParams, $location, Movie) {
        $scope.movie = Movie.get({ id: $routeParams.id });
        $scope.remove = function () {
            $scope.movie.$remove({ id: $routeParams.id }, function () {
                $location.path('/');
            });
        };
    };
    console.log('Controllers init');
})();