﻿/* 
After create this file you should run it in
Visual Studio Task Runner Explorer
*/
module.exports = function (grunt) {
    //load grunt plugins
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    //configure plugins
    grunt.initConfig({
        uglify: {
            my_target: {
                files: {
                    //take all *.js files from folder Scripts
                    'wwwroot/app.js': ['Scripts/app.js', 'Scripts/**/*.js']
                }
            }
        },
        //set watcher
        watch: {
            scripts: {
                files: ['Scripts/**/*.js'],
                tasks: ['uglify']
            }
        }
    });

    //define tasks
    grunt.registerTask('default', ['uglify', 'watch']);
};