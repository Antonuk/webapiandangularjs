﻿using Microsoft.Data.Entity;

namespace WebApiAndAngularJS.MoviesProject.Models
{
    public class MovieAppContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
    }
}