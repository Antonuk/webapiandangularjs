﻿using System.Collections.Generic;
using Microsoft.AspNet.Mvc;
using WebApiAndAngularJS.MoviesProject.Models;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;

namespace WebApiAndAngularJS.MoviesProject.API
{
    //api controller search methods using HTTP
    [Route("api/[controller]")]
    public class MoviesController : Controller
    {
        private readonly MovieAppContext _dbContext;

        public MoviesController(MovieAppContext context)
        {
            this._dbContext = context;
        }

        // GET: api/values
        [HttpGet]
        public async Task<IEnumerable<Movie>> Index()
        {
            /*
            return new List<Movie>
            {
                new Movie { Id = 1, Title = "Star Wars VI: The Force Awakening", Director = "Geoge Lucas" },
                new Movie { Id = 2, Title = "Deadpool", Director = "Marvel" },
                new Movie { Id = 3, Title = "Hateful Eight", Director = "Quentin Tarantino" }
            };
            */
            return await _dbContext.Movies.ToListAsync();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var movie = await _dbContext.Movies.FirstOrDefaultAsync(x => x.Id == id);
            if(movie == null)
            {
                return new HttpNotFoundResult();
            }
            else
            {
                return new ObjectResult(movie);
            }
            /*
            return "value";
            */
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Movie movie)
        {
            if(movie.Id == 0)
            {
                _dbContext.Movies.Add(movie);
                await _dbContext.SaveChangesAsync();
                return new ObjectResult(movie);
            }
            else
            {
                var original = _dbContext.Movies.FirstOrDefault(x => x.Id == movie.Id);
                original.Title = movie.Title;
                original.Director = movie.Director;
                await _dbContext.SaveChangesAsync();
                return new ObjectResult(original);
            }
            
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody]Movie movie)
        {
            _dbContext.Movies.Add(movie);
            await _dbContext.SaveChangesAsync();
            return new ObjectResult(movie);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var movie = _dbContext.Movies.FirstOrDefault(x => x.Id == id);
            _dbContext.Movies.Remove(movie);
            await _dbContext.SaveChangesAsync();
            return new HttpStatusCodeResult(200);
        }
    }
}
